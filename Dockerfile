FROM ubuntu:latest

WORKDIR /root

COPY general packages.txt extra.sh ./

RUN apt-get update && apt-get install -y $(cat packages.txt) && ./extra.sh
