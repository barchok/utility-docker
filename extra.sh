#!/bin/bash

wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64.tar.gz -O - |  tar xz && mv yq_linux_amd64 /usr/bin/yq


mkdir /root/.ssh
chmod 700 /root/.ssh
echo "Host *
  PubkeyAcceptedKeyTypes +ssh-rsa" > /root/.ssh/config


wget https://files.pythonhosted.org/packages/b9/dc/409aca9616032378c53b61ef07f46814b21e997b7746b4c836ab54e79b46/envtpl-0.7.2.tar.gz
tar -xf envtpl-0.7.2.tar.gz 
cd envtpl-0.7.2/
python3 setup.py install



echo 'ALL            ALL = (ALL) NOPASSWD: ALL' > /etc/sudoers.d/allowall