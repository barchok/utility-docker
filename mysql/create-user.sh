#!/bin/bash

echo "User for creation:"
read creationuser
echo "Password for creation:"
read -s creationpasswd
echo "Host:"
read host
echo "DB name:"
read database
echo "User:"
read user
echo "Password:"
read -s password

mysql -u${creationuser} -p${creationpasswd} -h"${host}" -e "CREATE DATABASE ${database} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
mysql -u${creationuser} -p${creationpasswd} -h"${host}" -e "CREATE USER ${user}@localhost IDENTIFIED BY '${password}';"
mysql -u${creationuser} -p${creationpasswd} -h"${host}" -e "GRANT ALL PRIVILEGES ON ${database}.* TO '${user}'@'%';"
mysql -u${creationuser} -p${creationpasswd} -h"${host}" -e "FLUSH PRIVILEGES;"

