#!/usr/bin/env python
import pika
import sys
import time
import sys

credentials = pika.PlainCredentials("","")
baserate = int(sys.argv[1])
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host="",credentials=credentials))
channel = connection.channel()





channel.queue_declare(queue='loadtest', durable=True)

counter = 0
countstart = 0
baserate = int(sys.argv[1])
rate = baserate
margin = 1000

while 1:
    if counter == rate:
        delta = int(round(time.time() * 1000) - countstart)
        print("Time Active For %r messages: %r" % (rate, delta - 1000))
        print("Time Per Message: %r ms" % ((delta-1000)/rate))
        counter = 0
        if delta < (1000 + margin):
            rate += baserate
            print("New rate: %r" % rate)
        print("####")
    if counter == 0:
        countstart = int(round(time.time() * 1000))
    message=str(int(round(time.time() * 1000)))
    channel.basic_publish(
    exchange='',
    routing_key='loadtest',
    body=message,
    properties=pika.BasicProperties(
        delivery_mode=2,
    ))
    time.sleep(1/rate)
    counter += 1
